# MAME Essentials #

This is a script to provide options and automate the process of the all killer no filler 
mame selections. BadMouth from the ArcadeControls.com forum began this project. A lot of 
credit is due to him for creating this catered list. You can see his work at the 
forum post where I got my data for this project from at 
(http://forum.arcadecontrols.com/index.php?topic=149708.0)[http://forum.arcadecontrols.com/index.php?topic=149708.0]

## What is All Killer, No Filler MAME Gamelists Directory? ##

BadMouth fro the arcade controls forum enlisted community help creating a catered list of the
best games available in mame for various genres. This selection trims your mame library down
to the games that are worth playing for being just good or introducing interesting or novel
mechanics. It significantly reduces the mame library to something more manageable.

## So what is MAME Essentials then? ##

This is just a script I wrote to automate this process some. It uses dialogs to allow you to
select which genres you want to grab and such. 

## Cool! So how do I use it? ##

I'll write instructions for this later, but essentially just download the project and run the
aknf.sh script. The dialogs should be pretty self explainatory. It will create a `_aknf` 
directory with all the files mame needs to run the games based on the selections you made.

Have fun!
