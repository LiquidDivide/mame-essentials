#!/bin/bash

#### DIALOG VALUES ####
BACK_TITLE="All-Killer-No-Filler"
HEIGHT=0
WIDTH=0
DIALOG_CANCEL=1
DIALOG_ESC=255

if [[ "$OSTYPE" == "darwin"* ]]; then
    SOURCE_PATH="/Users/$USER"
else
    SOURCE_PATH="/home/$USER";
fi
EXTENSION="zip";
DESTINATION="_aknf";

ON=on
OFF=off
YES=0
NO=1

#### OPTIONS VALUES ####
OPTION_COUNT=13

OPTION_ALTERNATES=0;
OPTION_BEAT_N_HACK=1;
OPTION_CLASSIC_HORZ=2;
OPTION_CLASSIC_VERT=3;
OPTION_OPTIONALS=4;
OPTION_PLATFORM=5;
OPTION_PUZZLE=6;
OPTION_RUN_N_GUN=7;
OPTION_SHMUP_HORZ=8;
OPTION_SHMUP_VERT=9;
OPTION_SPORTS=10;
OPTION_VS_FIGHTING=11;
OPTION_LIGHT_GUN=12

OPTION_VALUES=();

OPTION_NAMES=(\
    "Alternates" \
    "Beat 'N Hack" \
    "Classics Horizontal" \
    "Classics Vertical" \
    "Optionals" \
    "Platform" \
    "Puzzle" \
    "Run 'N Gun" \
    "SHMUPs Horizontal" \
    "SHMUPs Vertical" \
    "Sports" \
    "VS Fighters" \
    "Light Gun" \
);

OPTION_DESC=(\
    "-- Japanese versions of some roms." \
    "" \
    "" \
    "" \
    "-- See the documentation for more info." \
    "" \
    "" \
    "" \
    "" \
    "" \
    "" \
    "Requires a light gun to work (Aimtrak, Sinden Light Gun, etc.)" \
);

OPTION_PATHS=(\
    "./resources/alternates" \
    "./resources/beatNHack" \
    "./resources/classicHorz" \
    "./resources/classicVert" \
    "./resources/optional" \
    "./resources/platform" \
    "./resources/puzzle" \
    "./resources/runNGun" \
    "./resources/shmupHorz" \
    "./resources/shmupVert" \
    "./resources/sports" \
    "./resources/vsFighting" \
    "./resources/lightgun" \
);

addGamesFromFile() {
    input="$1"
    while IFS= read -r line
    do
        FILEMAP+=( "$line.$EXTENSION");
    done < "$input"
}

################## Unaccounted For BIOS Files ##################

#FILEMAP+=( "coh1000c.$EXTENSION" )
#FILEMAP+=( "coh1000t.$EXTENSION" )
#FILEMAP+=( "coh1002e.$EXTENSION" )
#FILEMAP+=( "coh1002m.$EXTENSION" )
#FILEMAP+=( "coh1002v.$EXTENSION" )
#FILEMAP+=( "coh3002c.$EXTENSION" )
#FILEMAP+=( "qsound_hle.$EXTENSION" )

generateFileList() {
    FILEMAP=()
    local index=0
    while [ "$index" -lt "$OPTION_COUNT" ]
    do
        if [ "${OPTION_VALUES[$index]}" = "$ON" ] ; then
            addGamesFromFile "${OPTION_PATHS[$index]}"
        fi
        ((++index))
    done

    FILEMAP=($(for file in "${FILEMAP[@]}"; do echo "$file";done| sort -u))
}

printFileMap() {
    for ELEMENT in ${FILEMAP[@]}; do echo file: "$ELEMENT"; done;
}

copyFileMap() { 
    local total=${#FILEMAP[@]};
    local destination="${SOURCE_PATH}/${DESTINATION}";
    local current=0;
    local percent=0;
    local copied=0;
    local failed=();
    echo
    echo
    [ ! -d "$destination" ] && mkdir -p "$destination"
    (
        for file in ${FILEMAP[@]}
        do
            local source="${SOURCE_PATH}/${file}";
            destination="${SOURCE_PATH}/${DESTINATION}/${file}";
            percent=$(( 100*(++current)/total ));
            echo "$percent"
            [[ -e "$source" ]] && cp "${source}" "${destination}"
            [ ! -f "${destination}" ] && failed+=($file);
        done
    ) \
    | dialog --title "Copying files" --gauge "" 8 80 0

    # TODO: This is not working. Need to fix above, since failed is empty.
    if [ ${#failed[@]} -gt 0 ] ; then
        dialog \
            --title "Failures" \
            --backtitle "$BACK_TITLE" \
            --infobox "${#failed[@]} files failed to copy. See log.txt for more info." "$HEIGHT" "$WIDTH"
    fi
}

clearOptions() {
    local index=0;
    while [ "$index" -lt "$OPTION_COUNT" ]
    do
        OPTION_VALUES["$index"]="$OFF";
        ((++index))
    done
}

resetOptions() {
    local index=0;
    while [ "$index" -lt "$OPTION_COUNT" ]
    do
        OPTION_VALUES["$index"]="$ON";
        echo "set OPTION_VALUES[$index] to ${OPTION_VALUES[$index]}"
        ((++index))
    done
    OPTION_VALUES["$OPTION_ALTERNATES"]="$OFF";
    OPTION_VALUES["$OPTION_OPTIONALS"]="$OFF";
}

showPathDialog() {
    exec 3>&1
    selection=$(dialog \
        --title "Select Full Romset Directory" \
        --backtitle "$BACK_TITLE" \
        --dselect "$SOURCE_PATH" 10 80 \
        2>&1 1>&3)
    local exit_status=$?
    exec 3>&-
    if [ "$exit_status" -eq "$DIALOG_CANCEL" ] || [ "$exit_status" -eq "$DIALOG_ESC" ] ; then
        return
    fi

    if [ -d "$selection" ]; then
        SOURCE_PATH="$selection"
    else
        dialog --infobox "Invalid directory selected. Select a valid directory" 4 80; 
        sleep 4;
        showPathDialog;
    fi
}

showExtensionDialog() {
    local zip=$OFF;
    local other=$OFF;
    [ "$EXTENSION" = "zip" ]  && zip=$ON;
    [ "$EXTENSION" = "7z" ] && other=$ON;
    
    exec 3>&1
    selection=$(dialog \
        --title "Extension options" \
        --backtitle "$BACK_TITLE" \
        --radiolist "Choose which extension your roms use by pressing spacebar when the option you want to select is highlighted. Enter key will select the options on the bottom." "$HEIGHT" "$WIDTH" 2 \
        "zip" "" "$zip" \
        "7z" "" "$other" \
        2>&1 1>&3)
    local exit_status=$?
    exec 3>&-
    if [ "$exit_status" -eq "$DIALOG_CANCEL" ] || [ "$exit_status" -eq "$DIALOG_ESC" ] ; then
        return
    fi
    EXTENSION="$selection"
}

showOptionsDialog() {
    exec 3>&1
    options=$(dialog \
        --title "Rom Options" \
        --backtitle "$BACK_TITLE" \
        --checklist "Toggle features using spacebar on the highlighted option. Up and down arrows move between options. Left and right arrows move between <OK> and <Cancel> below. Enter chooses the selected <OK> or <Cancel> Option." "$HEIGHT" "$WIDTH" 12 \
        "$OPTION_ALTERNATES" "${OPTION_NAMES[$OPTION_ALTERNATES]} ${OPTION_DESC[$OPTION_ALTERNATES]}" "${OPTION_VALUES[$OPTION_ALTERNATES]}" \
        "$OPTION_BEAT_N_HACK" "${OPTION_NAMES[$OPTION_BEAT_N_HACK]} ${OPTION_DESC[$OPTION_BEAT_N_HACK]}" "${OPTION_VALUES[$OPTION_BEAT_N_HACK]}" \
        "$OPTION_CLASSIC_HORZ" "${OPTION_NAMES[$OPTION_CLASSIC_HORZ]} ${OPTION_DESC[$OPTION_CLASSIC_HORZ]}" "${OPTION_VALUES[$OPTION_CLASSIC_HORZ]}" \
        "$OPTION_CLASSIC_VERT" "${OPTION_NAMES[$OPTION_CLASSIC_VERT]} ${OPTION_DESC[$OPTION_CLASSIC_VERT]}" "${OPTION_VALUES[$OPTION_CLASSIC_VERT]}" \
        "$OPTION_OPTIONALS" "${OPTION_NAMES[$OPTION_OPTIONALS]} ${OPTION_DESC[$OPTION_OPTIONALS]}" "${OPTION_VALUES[$OPTION_OPTIONALS]}" \
        "$OPTION_PLATFORM" "${OPTION_NAMES[$OPTION_PLATFORM]}" "${OPTION_VALUES[$OPTION_PLATFORM]}" \
        "$OPTION_PUZZLE" "${OPTION_NAMES[$OPTION_PUZZLE]} ${OPTION_DESC[$OPTION_PUZZLE]}" "${OPTION_VALUES[$OPTION_PUZZLE]}" \
        "$OPTION_RUN_N_GUN" "${OPTION_NAMES[$OPTION_RUN_N_GUN]} ${OPTION_DESC[$OPTION_RUN_N_GUN]}" "${OPTION_VALUES[$OPTION_RUN_N_GUN]}" \
        "$OPTION_SHMUP_HORZ" "${OPTION_NAMES[$OPTION_SHMUP_HORZ]} ${OPTION_DESC[$OPTION_SHMUP_HORZ]}" "${OPTION_VALUES[$OPTION_SHMUP_HORZ]}" \
        "$OPTION_SHMUP_VERT" "${OPTION_NAMES[$OPTION_SHMUP_VERT]} ${OPTION_DESC[$OPTION_SHMUP_VERT]}" "${OPTION_VALUES[$OPTION_SHMUP_VERT]}" \
        "$OPTION_SPORTS" "${OPTION_NAMES[$OPTION_SPORTS]} ${OPTION_DESC[$OPTION_SPORTS]}" "${OPTION_VALUES[$OPTION_SPORTS]}" \
        "$OPTION_VS_FIGHTING" "${OPTION_NAMES[$OPTION_VS_FIGHTING]} ${OPTION_DESC[$OPTION_VS_FIGHTING]}" "${OPTION_VALUES[$OPTION_VS_FIGHTING]}" \
        "$OPTION_LIGHT_GUN" "${OPTION_NAMES[$OPTION_LIGHT_GUN]} ${OPTION_DESC[$OPTION_LIGHT_GUN]}" "${OPTION_VALUES[$OPTION_LIGHT_GUN]}" \
        2>&1 1>&3)
    local exit_status=$?
    exec 3>&-
    if [ "$exit_status" -eq "$DIALOG_CANCEL" ] || [ "$exit_status" -eq "$DIALOG_ESC" ] ; then
        return
    fi
    clearOptions
    options=($options)
    for option in "${options[@]}"
    do
        OPTION_VALUES["$option"]="$ON"
    done;
}

showExecuteDialog() {
    generateFileList
    local text="Are you sure you want to execute with the current settings?\n\n\
    Up to ${#FILEMAP[@]} files will be copied.\n\n
Destination: "$SOURCE_PATH"/$DESTINATION";

    local index=0;
    while [ "$index" -lt "$OPTION_COUNT" ]
    do
        [ "${OPTION_VALUES[$index]}" = "$ON" ] && text="$text\nInclude ${OPTION_NAMES[$index]}";
        ((++index));
    done
    
    dialog --title "Execute" \
        --backtitle "$BACK_TITLE" \
        --defaultno \
        --yesno "$text" "$HEIGHT" "$WIDTH"
    response=$?
    if [ "$response" -eq "$YES" ] ; then
        copyFileMap
    fi
}

showMainMenu() {
    while true; do
        exec 3>&1;
        selection=$(dialog \
            --title "Main Menu" \
            --backtitle "$BACK_TITLE" \
            --menu "Choose an option to configure. Up and down arrows change selection. Use the Enter key to make a selection." "$HEIGHT" "$WIDTH" 5 \
            0 "Set full romset path" \
            1 "Configure file extension" \
            2 "Options" \
            3 "Execute with current config" \
            2>&1 1>&3);
        local exit_code=$?
        exec 3>&-;

        if [ "$exit_code" = "$DIALOG_CANCEL" ] || [ "$exit_code" = "$DIALOG_ESC" ] ; then
            clear;
            echo "Exiting program";
            exit 1;
        fi
        case "$selection" in
            0)
                showPathDialog
                ;;
            1)
                showExtensionDialog
                ;;
            2)
                showOptionsDialog
                ;;
            3)
                showExecuteDialog
                ;;
        esac
    done
}

resetOptions
showMainMenu
